from flask import Flask, abort, request
import re
import dns.resolver
import smtplib

app = Flask(__name__)

# Create an SMTP client globally to re-use the connection if possible
server = smtplib.SMTP()


@app.route('/validate', methods=['POST'])
def validate():
    email = request.get_json().get('email', '')
    email = email.lower().strip()

    # Do some basic regex validation first
    match = re.match('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$', email)
    if not match:
        abort(400, {'message': 'Email not match'})

    # Extract the host
    host = email.split('@')[1]

    # Get the MX record so we can get the SMTP connection afterwards
    try:
        records = dns.resolver.query(host, 'MX')
        mx_record = str(records[0].exchange)
    except dns.exception.DNSException:
        # DNS record couldn't be found!
        abort(400, {'message': 'DNS exception'})

    # SMTP Conversation
    server.connect(mx_record)
    server.helo(host)
    server.mail(email)
    code, message = server.rcpt(str(email))
    server.quit()

    # Assume 250 as Success
    if code != 250:
        abort(400, {'message': 'SMTP not respond 250'})

    return "", 200


# We only need this for local development.
if __name__ == '__main__':
    app.run()
